<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group([ 
//     'middleware'    => 'api',
//     'prefix'        => 'auth',
//     'namespace'     => 'Auth'
// ], function () {
    
// });
Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('verification', 'VerificationController');
    Route::post('regenerate_otp', 'RegenerateOtpController');
    Route::post('update_pass', 'UpdatePasswordController');
    Route::post('login', 'LoginController');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');
    Route::post('logout', 'LogoutController')->middleware('auth:api');

    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');

});

Route::get('get_profile', 'ProfileController@index');
Route::post('update_profile', 'ProfileController@update');

Route::group([
    'prefix' => 'campaign'
], function () {
    Route::get('/', 'CampaignController@index');
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/{id}', 'CampaignController@detail');
    Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
    'prefix' => 'blog'
], function () {
    Route::get('/', 'BlogController@index');
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
    Route::get('/{id}', 'BlogController@detail');
});

// Route::get('user', 'UserController');
