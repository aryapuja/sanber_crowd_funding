<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('app');
});

Route::any('/{any}', function () {
    return view('app');
});

// Route::get('/tes', 'testController@index');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware('auth')->group(function(){
//     Route::get('/route-1', 'testController@route1');
//     Route::get('/route-2', 'testController@route2');
// });