<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Roles extends Model 
{
    use UsesUuid;
    protected $primaryKey = 'id_role';

    protected $guarded = [];
}
