<?php

namespace App\Mail;

use App\otp_codes;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $otp_code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, otp_codes $otp_code)
    {
        $this->user = $user;
        $this->otp_code = $otp_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('newRegister@example.com')
                    ->view('send_email_user_registered')
                    ->with([
                        'name'      => $this->user->name,
                        'otp'       => $this->otp_code->otp,
                    ]);
    }
}
