<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Traits\UsesUuid;
use Carbon\Carbon;
use Facade\FlareClient\Stacktrace\Stacktrace;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;
    protected $primaryKey = 'id_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo', 'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot(){
        parent::boot();

        Static::creating(function($model){
            $model->id_role = $model->get_user_role_id();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });
    }

    public function cekAdmin(){
        if($this->id_role == '1b729a2f-9e46-40ad-b8a9-7ded237cb86c'){
            return true;
        }
        return false;
    }

    public function verifEmail(){
        if(is_null($this->email_verified_at)){
            return false;
        }
        return true;
    }

    public function get_user_role_id()
    {
        $role = Roles::where('nama_role','user')->first();
        return $role->id_role;
    }

    public function generate_otp_code()
    {
        do{
            $random = mt_rand(100000,999999);
            $check  = otp_codes::where('otp',$random)->first();
        }while($check);

        $now = Carbon::now();
        $otp_code =otp_codes::updateOrCreate(
            ['id_user'  => $this->id_user],
            ['otp'      => $random, 'expired'  => $now->addMinute(5)]
        );
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
