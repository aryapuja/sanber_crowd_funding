<?php

namespace App\Listeners;

use App\Events\UserRegenerateOtpEvent;
use App\Mail\RegenerateOtpMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegenerateOtpEvent  $event
     * @return void
     */
    public function handle(UserRegenerateOtpEvent $event)
    {
        Mail::to($event->user)->send(new RegenerateOtpMail($event->user,$event->otp_code));
    }
}
