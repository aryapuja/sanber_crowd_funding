<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class testController extends Controller
{

    public function __construct() {
        $this->middleware('verif_email');
        $this->middleware('akun_admin');
    }

    public function index()
    {
        return 'Tes testController';
    }

    public function route1()
    {
        // dd(Auth::user());
        return 'Berhasil masuk karena sudah melakukan verifikasi email';
    }

    public function route2()
    {
        return 'Berhasil masuk karena role akun adalah admin';
    }
}
