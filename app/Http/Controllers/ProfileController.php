<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');   
    }

    public function index()
    {
        User::where('email', request('email'))->first();

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Profile Berhasil Ditampilkan',
            'data'              => auth()->user(),
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $newData = User::where('id_user', auth()->user()->id_user)->first();
        $newData->name  = $request->name;

        $nama_file='';
        if ($request->file('photo')) {

            $file = $request->file('photo');
            $tujuan_upload = '/users/photo-profile/';
            $nama_file = $newData->id_user."-".time()."-".$file->getClientOriginalName();
            $file->move(public_path($tujuan_upload),$nama_file);
            $newData->photo = $tujuan_upload.$nama_file;
        }
        $newData->save();
        
        // return $nama_file;
        return response()->json([
            'response_code'     => '00',
            "message"           => "Data Berhasil di Upload",
            'data'              => $newData,
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
