<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\VerificationRequest;
use App\otp_codes;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(VerificationRequest $request)
    {
        $otp_code = otp_codes::where('otp', $request->otp)->first();

        //code otp tidak ada
        if(!$otp_code){
            return response()->json([
                'response_code'     => '01',
                'response_message'  => 'Kode OTP Tidak Ditemukan'
            ], 200);
        }

        //code otp kadaluarsa
        $now = Carbon::now();
        if($now > $otp_code->expired){
            return response()->json([
                'response_code'     => '01',
                'response_message'  => 'Kode OTP Sudah Kadaluarsa, Silahkan generate ulang code otp'
            ], 200);
        }

        //code otp benar, update email verified user
        $user = User::find($otp_code->id_user);
        $user->email_verified_at = Carbon::now();
        $user->save();
        $otp_code->delete();

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Verifikasi Akun Berhasil Dilakukan',
            'data'              => $user
        ], 200);
    }
}
