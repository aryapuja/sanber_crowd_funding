<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email'     => 'required',
            'password'  => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if(!$token = auth()->attempt($credentials)){
            return response(['error' => 'Email atau Password Salah'], 401);
        };

        $infoUser = User::where('email', request('email'))->first();
        if(!$infoUser){
            return response()->json([
                'response_code'     => '02',
                'response_message'  => 'email tidak terdaftar',
            ], 200);
        }

        $data['token'] = $token;
        $data['infoUser'] = $infoUser;

        // return response()->json(compact('token'));
        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'data campaigns berhasil ditampilkan',
            'data'              => $data
            // 'token'             => $token,
            // 'user'              => $infoUser
        ], 200);
    }
}
