<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\User;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdatePasswordRequest $request)
    {
        $infoUser = User::where('email', request('email'))->first();
        if(!$infoUser){
            return response()->json([
                'response_code'     => '02',
                'response_message'  => 'email tidak terdaftar',
            ], 200);
        }

        if(is_null($infoUser->email_verified_at)){
            return response()->json([
                'response_code'     => '02',
                'response_message'  => 'email belum di verifikasi.',
            ], 200);
        }

        User::where('email', $request->email)
                ->update(['password' => bcrypt(request('password'))]);
        // $infoUser->password = bcrypt(request('password'));
        // $infoUser->save();
        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Password Berhasil Diubah',
            'data'              => $infoUser
        ], 200);

    }
}
