<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        // dd($url);
        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        try{
            $social_user = Socialite::driver($provider)->stateless()->user();
            
            if(!$social_user){
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'login failed -> social user failed'
                ], 401);
            }
            // dd($social_user);
            
            $user=User::whereEmail($social_user->email)->first();
            
            if(!$user){
                if($provider == 'google'){
                    $photo = $social_user->avatar;
                }
                $user = User::create([
                    'email'             => $social_user->email,
                    'name'              => $social_user->name,
                    'email_verified_at' => Carbon::now(),
                    'photo'             => $photo
                    ]);
                }
                // dd($user);
            $data['user'] = $user;
            $data['token'] = auth()->login($user);

            return response()->json([
                'response_code'     => '00',
                'response_message'  => 'login Success',
                'data'              => $data
            ], 401);

        }catch(\Throwable $th){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'login failed -> gagal'
            ], 401);
        }
    }
}
