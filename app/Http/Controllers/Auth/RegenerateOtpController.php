<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\otp_codes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegenerateOtpRequest;
use Illuminate\Http\Request;
use App\Events\UserRegenerateOtpEvent;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegenerateOtpRequest $request)
    {
        $infoUser = User::where('email', request('email'))->first();

        if(!$infoUser){
            return response()->json([
                'response_code'     => '03',
                'response_message'  => 'email tidak terdaftar',
            ], 200);
        }

        $infoUser->generate_otp_code();
        $otp_code = otp_codes::where('id_user', $infoUser->id_user)->first();

        event(new UserRegenerateOtpEvent($infoUser,$otp_code));
        $data['user'] = $infoUser;

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Silahkan Cek Email',
            'data'              => $infoUser
        ], 200);
    }
}
