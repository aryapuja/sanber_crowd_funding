<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\otp_codes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $data=[];
        $data_request = $request->all();
        $newRegis = User::create($data_request);
        $otp_code = otp_codes::where('id_user',$newRegis->id_user)->first();

        event(new UserRegisteredEvent($newRegis,$otp_code));
        $data['user'] = $newRegis;

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Silahkan Cek Email',
            'data'              => $data
        ], 200);
    }
}
