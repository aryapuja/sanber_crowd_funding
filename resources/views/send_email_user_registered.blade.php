<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Selamat, Akun anda dengan nama {{$name}} sudah terdaftar di aplikasi kami</p>
    <p>Untuk melakukan verifikasi email, gunakan kode otp: {{ $otp }}. Harap menjaga kode otp anda dan jangan disebarkan ke orang lain. </p>
</body>
</html>