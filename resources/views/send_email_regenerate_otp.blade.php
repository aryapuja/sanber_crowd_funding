<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Halo {{$name}}, Kode OTP mu yang baru adalah: {{ $otp }} </p>
    <p>Segera masukkan kode tersebut sebelum {{ $expired }} atau anda harus me-regenerate kode OTP lagi.</p>
    <p>Harap menjaga kode otp anda dan jangan disebarkan ke orang lain. </p>
</body>
</html>