<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
                'id_role'       => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'nama_role'     => 'admin',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

        DB::table('roles')->insert([
                'id_role'       => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'nama_role'     => 'user',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);
    }
}
